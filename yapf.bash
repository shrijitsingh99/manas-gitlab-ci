#!/bin/bash
set -e

# Find all Python files recurively and run yapf
#-------------------------------------------------------------------------------
#yapf3 -pr > /dev/null 2>&1 &

# Show changes in formatted code
echo "Showing changes in expected code style:"
git --no-pager diff

# Check if there have been any changes in the repo
#-------------------------------------------------------------------------------
if ! git diff-index --quiet HEAD --; then
    echo "yapf test failed: changes required to comply to formatting rules. See diff above.";
    exit 1
fi

# Code is formatted correctly
#-------------------------------------------------------------------------------
echo "Passed yapf test"
exit 0

